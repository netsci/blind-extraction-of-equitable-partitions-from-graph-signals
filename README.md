# Blind extraction of equitable partitions from graph signals
This repository contains the code to replicate our results from the publication

M. Scholkemper and M. T. Schaub, "Blind Extraction of Equitable Partitions from Graph Signals," ICASSP 2022 - 2022 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP), Singapore, Singapore, 2022, pp. 5832-5836, doi: 10.1109/ICASSP43922.2022.9746676.

The extended paper is availible here and on arxiv
https://arxiv.org/pdf/2203.05407.pdf

## Code Installation

Simply run `pip install -r code/requirements.txt` to install the required python packages or install them manually.

## Code Usage

To replicate the results of our paper, use the Experiments.ipynb, which will guide you through the process that lead to the figures displayed in the paper.
